## Merge changes with vimdiff
Credit: https://stackoverflow.com/a/31686752

You can use the following basic commands to merge:  
`d` `o` - Get changes from other window into the current window  
`d` `p` - Put the changes from current window into the other window  
`]` `c` - Jump to the next change  
`[` `c` - Jump to the previous change  
`z` `o` - Open folded lines  
`z` `c` - Close folded lines  
`z` `r` - Unfold both files completely  
`z` `m` - Fold both files completely  

`Ctrl` `w` `w` - change active window

`:only | wq` - quit other windows, write and quit

### Quirks to watch for  
Both do and dp work if you are on a block of change (or just one line under a single line of change) in Normal mode, but not in Visual mode.

The undo command will only work in the buffer that was changed, so if you use `d` `p` and change your mind, you need to switch to the other buffer to undo.

`:diffupdate` will re-scan the files for changes (Vim can get confused, and show bogus stuff).

### Visual mode and finer grained control
When selecting lines of text in Visual mode, you must use the normal commands:
`:'<,'>diffget` and `:'<,'>diffput`.

For example:
Enter Visual mode and mark some text/lines.
Then type `:diffput` to push the selected lines to the other file or `:diffget` to get the selected lines from the other file.
To belabor the point: This means that if there is a block of changes consisting of multiple lines, then selecting a subset of lines and issueing `:diffput` will only apply those changes in the other buffer.
(`:diffget` and `:diffput` also accept ranges, see `:h copy-diffs` for more.)

### Compare two buffers inside Vim
If you load up two files in splits (`:vs` or `:sp`), you can do `:diffthis` on each window and achieve a diff of files that were already loaded in buffers.

`:diffoff` can be used to turn off the diff mode.

This [Vimcastsi](http://vimcasts.org/episodes/fugitive-vim-resolving-merge-conflicts-with-vimdiff/) post and video show this in practice.

### How to apply all changes between buffers
Make sure that all participating buffers are in diff mode (see `:h start-vimdiff`)

a. Get changes from a buffer to the current one: `:%diffget <buffer-number>`

b. Put all changes from current buffer into another: `:%diffput <buffer-number>`

(`:%` is a range to select the entire file; see `:h :%`. `:ls` will show currently opened buffers.)

## Paste clipboard without autoindent
`:set paste`
