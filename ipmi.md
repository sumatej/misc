# IPMI Tips & Tricks

Most of the IPMI commands mentioned here will requite to have installed package `ipmitool` on your system.
You can run `ipmitool` command on local system if your node is running Linux or you can run it on a remote node. In case of a remote node, your command will start with:
```
ipmitool -H <ipmi_ip_or_hostname> -U ADMIN -P ADMIN
```
Where password (specified by `-P`) can be different on some nodes.

Some nodes needs also `-I lanplus` to connect from other node.

To access IPMI iKVM (Keyboard, Video, Mouse, over IP) you can try to access in your web browser https://<ipmi_ip_or_hostname>:443/ or download IPMIview tool from https://www.supermicro.com/en/solutions/management-software/ipmi-utilities.

#### Set IPMI network interface to DHCP
On a node where you have Linux running, you can configure IPMI interface to use DHCP via:
```
ipmitool lan set 1 ipsrc dhcp
```

#### Set IPMI network interface to static IP
On a node where you have Linux running, you can configure IPMI interface static IP via:
```
ipmitool lan set 1 ipsrc static
ipmitool lan set 1 ipaddr 10.0.0.2
ipmitool lan set 1 netmask 255.255.255.0
ipmitool lan set 1 defgw ipaddr 10.0.0.1
```

#### Set PXE boot on next boot
```
ipmitool chassis bootdev pxe
```

#### Set password for user ADMIN (uid: 2) to ADMIN
```
ipmitool user set password 2 ADMIN
```

#### Get the first NIC MAC address
```
ipmitool raw 0x30 0x21 | tail -c 18 | tr ' ' ':'
```

#### Cold reset of the node
Almost like to plug-out and plug-in power cord
```
ipmitool bmc reset cold
```

#### Get SOL console
```
ipmitool sol activate

# or use ipmiconsole
ipmiconsole -h <ipmi_ip_or_hostname> -u ADMIN -p ADMIN
```

#### Get and clear the System Event Log (SEL)
If SEL is not cleared it can get full and cause some warnings. To see the events and clear them use:
```
ipmitool sel list
ipmitool sel clear
```

#### Use jumpbox for web UI
If you are able to run ssh command on a computer with web browser, then you can use jumphost to access IPMI web UI from a network to which you have no direct access.
Only trouble, caused by the port 443, is that you have to run it on your node and also on a jumphost as root. So at least at time of logini, you need to enable it.
```
jumphost_ip=
ipmi_ip=135.227.66.75
sudo -E ssh root@${jumphost_ip} -L127.0.0.1:443:${ipmi_ip}:443 -L127.0.0.1:5900:${ipmi_ip}:5900 -L127.0.0.1:5901:${ipmi_ip}:5901 -L127.0.0.1:5120:${ipmi_ip}:5120 -L127.0.0.1:5123:${ipmi_ip}:5123 -N```


IPMIView tool will not work with this port forwarding as it uses UDP port 623, but with this we can forward only TCP ports.